#include <stdio.h>

#include "ESP8266WebServer.h"

void writeWebServerOutput(const char* const str) {
    printf("%s", str);
}

void route1handler(const WebServerRoute* route, const WebRequest* req, WebResponse* rep) {
    ESP8266WebServer_WriteResponse(rep, STATUS_OK, CONTENTTYPE_PLAIN, "Hello");
    printf(" >> >> [ROUTE 1 HANDLER]\r\n");
}

void routeOnHandler(const WebServerRoute* route, const WebRequest* req, WebResponse* rep) {
    ESP8266WebServer_WriteResponse(rep, STATUS_OK, CONTENTTYPE_PLAIN, "OKOKOKOKOK");
    int ledN;
    if (ESP8266WebServer_TestMatchAndGetParameter(route, req, 1, &ledN))
        printf(" >> >> [LED %d ON]\r\n", ledN);
}

int main() {

    ESP8266WebServer espServer;
    ESP8266WebServer_InitServer(&espServer, &writeWebServerOutput);
    ESP8266WebServer_AddRoute(&espServer, "/on/:i", METHOD_GET | METHOD_POST, &routeOnHandler);
    ESP8266WebServer_Start(&espServer, 80);

    ESP8266WebServer_HandleSingleLineInput(&espServer, "0,CONNECTED\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "+IPD,0,57:GET /on/1?x=y HTTP/1.1\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "Connection: Keep-Alive\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "test=asdf\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "1,CONNECTED\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "+IPD,1,57:GET /on/2 HTTP/1.1\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "Connection: Keep-Alive\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "test=asdf\r\n");
    ESP8266WebServer_HandleNextAvailableRequest(&espServer);
    ESP8266WebServer_HandleNextAvailableRequest(&espServer);

    /*ESP8266WebServer_HandleSingleLineInput(&espServer, "+IDP,1,18:GET /on HTTP/1.1\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "\r\n");

    ESP8266WebServer_HandleSingleLineInput(&espServer, "+IDP,0,19:POST /on HTTP/1.1\r\n");
    ESP8266WebServer_HandleSingleLineInput(&espServer, "\r\n");*/

    ESP8266WebServer_Stop(&espServer);

    return 0;

}