#ifndef ESP8266WEBSERVER_H
#define ESP8266WEBSERVER_H

#include "ESP8266WebServerConfig.h"

/* Request */

typedef enum _WebRequestRawState {
    EMPTY,
    CONNECTED,
    CONNECTED__RECEIVED_FIRST_LINE,
    CONNECTED__RECEIVED_HEADERS,
    CONNECTED__SERVABLE
} WebRequestRawState;

typedef struct _WebRequestRaw {
    unsigned int connId;
    WebRequestRawState state;
    char firstLine[MAX_LENGTH_OF_REQ_FIRST_LINE + 1];
#ifdef GET_REQUEST_BODY
    char body[MAX_LENGTH_OF_REQ_BODY + 1];
#endif
    unsigned int length;
    int remainingLength;
    unsigned int /* BOOL */ tooLong;
} WebRequestRaw;

typedef enum _WebRequestMethod {
    METHOD_UNKNOWN = 0,
    METHOD_GET = 1,
    METHOD_POST = 2
} WebRequestMethod;

typedef struct _WebRequest {
    WebRequestMethod method;
    char url[MAX_LENGTH_OF_REQ_URL + 1];
#ifdef GET_REQUEST_BODY
    char body[MAX_LENGTH_OF_REQ_BODY + 1];
#endif
} WebRequest;

/* Response */

typedef enum _WebResponseStatusCode {
    STATUS_OK = 200,
    STATUS_BADREQUEST = 400,
    STATUS_FORBIDDEN = 403,
    STATUS_NOTFOUND = 404,
    STATUS_INTERNALERROR = 500
} WebResponseStatusCode;

typedef enum _WebResponseContentType {
    CONTENTTYPE_PLAIN,
    CONTENTTYPE_JSON,
    CONTENTTYPE_XML
} WebResponseContentType;

typedef struct _WebResponse {
    WebResponseStatusCode statusCode;
    WebResponseContentType contentType;
#ifdef RESPONSE_HAS_BODY
    char body[MAX_LENGTH_OF_REP_BODY + 1];
    unsigned int /* BOOL */ tooLong;
#endif
} WebResponse;

/* Route */

struct _WebServerRoute;
typedef struct _WebServerRoute WebServerRoute;

typedef void (* WebRequestHandler)(const WebServerRoute*, const WebRequest*, WebResponse*);

struct _WebServerRoute {
    char url[MAX_LENGTH_OF_ROUTE_URL + 1];
    WebRequestMethod method;
    WebRequestHandler handler;
};

/* Server */

typedef void (* WebServerOutputWriter)(const char*);

typedef struct _ESP8266WebServer {
    int currentRawRequest;
    WebRequestRaw rawRequests[COUNT_OF_SIMULTANEOUS_CONNECTIONS];
    unsigned int requestPriorities[COUNT_OF_SIMULTANEOUS_CONNECTIONS];
    WebServerRoute routes[MAX_ROUTE_COUNT_PER_SERVER];
    unsigned int routeCount;
    WebServerOutputWriter outputWriter;
} ESP8266WebServer;

void ESP8266WebServer_InitServer(ESP8266WebServer* server, WebServerOutputWriter outputWriter);


// Functions

void ESP8266WebServer_Start(ESP8266WebServer* server, unsigned int port);
void ESP8266WebServer_Stop(ESP8266WebServer* server);

int ESP8266WebServer_AddRoute(ESP8266WebServer* server, const char* url, WebRequestMethod method, WebRequestHandler handler);
void ESP8266WebServer_ClearRoutes(ESP8266WebServer* server);

void ESP8266WebServer_HandleSingleLineInput(ESP8266WebServer* server, const char* line);
int ESP8266WebServer_HandleNextAvailableRequest(ESP8266WebServer* server);
int ESP8266WebServer_HandleRequest(ESP8266WebServer* server, unsigned int connectionId);
int ESP8266WebServer_TestMatchAndGetParameter(const WebServerRoute* route, const WebRequest* request, unsigned int paramIndex, int* paramValue);

#ifdef RESPONSE_HAS_BODY
void ESP8266WebServer_WriteResponse(WebResponse* response, WebResponseStatusCode statusCode, WebResponseContentType contentType, const char* body);
#else
void ESP8266WebServer_WriteResponse(WebResponse* response, WebResponseStatusCode statusCode, WebResponseContentType contentType);
#endif

#endif // ESP8266WEBSERVER_H