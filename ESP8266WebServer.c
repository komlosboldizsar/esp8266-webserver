#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "ESP8266WebServerConfig.h"
#include "ESP8266WebServer.h"
#include "stringext.h"

/* Constants */

#ifdef USE_REASON_PHRASES_IN_RESPONSE
static const char const REASON_PHRASE_OK[] = "OK";
static const char const REASON_PHRASE_BADREQUEST[] = "Bad Request";
static const char const REASON_PHRASE_FORBIDDEN[] = "Forbidden";
static const char const REASON_PHRASE_NOTFOUND[] = "Not Found";
static const char const REASON_PHRASE_INTERNALERROR[] = "Internal Server Error";
#endif

#ifdef USE_CONTENT_TYPE_HEADER_IN_RESPONSE
static const char const CONTENT_TYPE_PLAIN[] = "text/plain";
static const char const CONTENT_TYPE_JSON[] = "application/json";
static const char const CONTENT_TYPE_XML[] = "text/xml";
#endif

static const char const CRLF[] = "\r\n";
static const char const EMPTY_STR[] = "";

static const char const AT_CMD_CIPMUX_1[] = "AT+CIPMUX=1\r\n";
static const char const AT_CMD_CIPSERVERMAXCONN_1[] = "AT+CIPSERVERMAXCONN=1\r\n";
static const char const AT_CMD_CIPSERVER_1_x[] = "AT+CIPSERVER=1,%u\r\n";
static const char const AT_CMD_CIPSTO_1[] = "AT+CIPSTO=2\r\n";
static const char const AT_CMD_CIPSERVER_0[] = "AT+CIPSERVER=0\r\n";
static const char const AT_CMD_CIPSEND_x_x[] = "AT+CIPSEND=%d,%lu\r\n";
static const char const AT_CMD_CIPCLOSE_x[] = "AT+CIPCLOSE=%d\r\n";
static const char const IDP[] = "+IPD,";
static const char const xCONNECT[] = ",CONNECT";
static const char const xCLOSED[] = ",CLOSED";

static const char const RESPONSE_FIRST_LINE[] = "HTTP/1.1 %d %s\r\n";
static const char const HEADER_CONTENT_TYPE[] = "Content-Type: %s\r\n";

static const char const METHOD_STR_GET[] = "GET";
static const char const METHOD_STR_POST[] = "POST";

/* Static (private) function declarations */

static void ESP8266WebServer_InitRequest(WebRequest* request, WebRequestRaw* requestRaw);
static void ESP8266WebServer_InitResponse(WebResponse* response);
static void ESP8266WebServer_InitRawRequest(WebRequestRaw* requestRaw, unsigned int connId);
static void ESP8266WebServer_ClearRawRequest(WebRequestRaw* requestRaw);

static void ESP8266WebServer_OutputResponse(ESP8266WebServer* server, const WebRequestRaw* rawRequest, WebResponse* response);
static int ESP8266WebServer_TestRouteMatch(WebServerRoute* route, WebRequest* request);

/* Definitions */

void ESP8266WebServer_InitServer(ESP8266WebServer* server, WebServerOutputWriter outputWriter) {
    server->currentRawRequest = -1;
    server->routeCount = 0;
    server->outputWriter = outputWriter;
    unsigned int i;
    for (i = 0; i < COUNT_OF_SIMULTANEOUS_CONNECTIONS; i++) {
        server->requestPriorities[i] = i;
        ESP8266WebServer_InitRawRequest(&server->rawRequests[i], i);
    }
}

static void ESP8266WebServer_InitRawRequest(WebRequestRaw* requestRaw, unsigned int connId) {
    requestRaw->connId = connId;
    requestRaw->state = EMPTY;
    requestRaw->length = 0;
    requestRaw->remainingLength = 0;
#ifdef GET_REQUEST_BODY
    requestRaw->body[0] = '\0';
#endif
    requestRaw->firstLine[0] = '\0';
    requestRaw->tooLong = 0;
}

static void ESP8266WebServer_ClearRawRequest(WebRequestRaw* requestRaw) {
    ESP8266WebServer_InitRawRequest(requestRaw, requestRaw->connId);
}

void ESP8266WebServer_Start(ESP8266WebServer* server, unsigned int port) {
    char atCmd[40];
    server->outputWriter(AT_CMD_CIPMUX_1);
    server->outputWriter(AT_CMD_CIPSERVERMAXCONN_1);
    sprintf(atCmd, AT_CMD_CIPSERVER_1_x, port);
    server->outputWriter(atCmd);
    server->outputWriter(AT_CMD_CIPSTO_1);
}

void ESP8266WebServer_Stop(ESP8266WebServer* server) {
    server->outputWriter(AT_CMD_CIPSERVER_0);
}

int ESP8266WebServer_AddRoute(ESP8266WebServer* server, const char* url, WebRequestMethod method, WebRequestHandler handler) {

    if (server->routeCount >= MAX_ROUTE_COUNT_PER_SERVER)
        return 0;

    WebServerRoute* newRoute = &server->routes[server->routeCount];
    strlimcpy(newRoute->url, url, MAX_LENGTH_OF_ROUTE_URL);
    newRoute->method = method;
    newRoute->handler = handler;

    server->routeCount++;

    return 1;

}

void ESP8266WebServer_ClearRoutes(ESP8266WebServer* server) {
    server->routeCount = 0;
}

void ESP8266WebServer_HandleSingleLineInput(ESP8266WebServer* server, const char* line) {

    if ((line[0] == '>') && (line[1] == '\r') && (line[2] == '\n') && (line[3] == '\0'))
        return;

    if ((line[1] == ',') && strncmp(&line[1], xCONNECT, (sizeof(xCONNECT) - 1)) == 0) {
        unsigned int connectionId = (unsigned int) (line[0] - '0');
        server->rawRequests[connectionId].state = CONNECTED;
        return;
    }

    if ((line[1] == ',') && strncmp(&line[1], xCLOSED, (sizeof(xCLOSED) - 1)) == 0) {
        unsigned int connectionId = (unsigned int) (line[0] - '0');
        server->rawRequests[connectionId].state = EMPTY;
        server->currentRawRequest = -1;
        return;
    }

    if ((line[4] == ',') && strncmp(line, IDP, (sizeof(IDP) - 1)) == 0) {

        unsigned int connectionId = (unsigned int) (line[5] - '0');
        if (connectionId >= COUNT_OF_SIMULTANEOUS_CONNECTIONS)
            return;
        server->currentRawRequest = connectionId;

        WebRequestRaw* rawRequest = &server->rawRequests[connectionId];

        rawRequest->state = CONNECTED__RECEIVED_FIRST_LINE;
#ifdef GET_REQUEST_BODY
        rawRequest->body[0] = '\0';
#endif
        char* firstLine = strchr(line, ':') + 1;
        if (!strlimcpy(rawRequest->firstLine, firstLine, MAX_LENGTH_OF_REQ_FIRST_LINE))
            rawRequest->tooLong = 1;
        int requestLength = atoi(&line[7]); // NOLINT
        rawRequest->length = (unsigned int) requestLength;
        rawRequest->remainingLength = requestLength - (int) strlen(firstLine);

        return;

    }

    if (server->currentRawRequest < 0)
        return;
    WebRequestRaw* rawRequest = &server->rawRequests[server->currentRawRequest];

    WebRequestRawState state = (volatile WebRequestRawState) rawRequest->state;
    switch (state) {

        case CONNECTED__RECEIVED_FIRST_LINE: { // Receiving headers
            rawRequest->remainingLength -= strlen(line);
            if (strcmp(line, CRLF) == 0)
                rawRequest->state = CONNECTED__RECEIVED_HEADERS;
        }
            break;

        case CONNECTED__RECEIVED_HEADERS: { // Receiving body
            rawRequest->remainingLength -= strlen(line);
#ifdef GET_REQUEST_BODY
            if (!strlimcat(rawRequest->body, line, MAX_LENGTH_OF_REQ_BODY))
                rawRequest->tooLong = 1;
#endif
        }
            break;

        case EMPTY:
        case CONNECTED:
        case CONNECTED__SERVABLE:
        default:
            break;

    }

    if (rawRequest->remainingLength <= 0)
        if ((state == CONNECTED__RECEIVED_FIRST_LINE) || (state == CONNECTED__RECEIVED_HEADERS))
            rawRequest->state = CONNECTED__SERVABLE;

}

int ESP8266WebServer_HandleNextAvailableRequest(ESP8266WebServer* server) {

    int queueIndex = -1;
    unsigned int q;
    for (q = 0; q < COUNT_OF_SIMULTANEOUS_CONNECTIONS; q++) {
        unsigned int connectionId = server->requestPriorities[q];
        if ((volatile WebRequestRawState) server->rawRequests[connectionId].state == CONNECTED__SERVABLE) {
            queueIndex = q;
            break;
        }
    }

    if (queueIndex < 0)
        return 0;

    unsigned int connectionId = server->requestPriorities[queueIndex];
    ESP8266WebServer_HandleRequest(server, connectionId);

    // Set priority
    unsigned int i;
    for (i = (unsigned int) queueIndex; i < COUNT_OF_SIMULTANEOUS_CONNECTIONS - 1; i++)
        server->requestPriorities[i] = server->requestPriorities[i + 1];
    server->requestPriorities[COUNT_OF_SIMULTANEOUS_CONNECTIONS - 1] = connectionId;

    return 1;

}

int ESP8266WebServer_HandleRequest(ESP8266WebServer* server, unsigned int connectionId) {

    WebResponse response;
    ESP8266WebServer_InitResponse(&response);

    WebRequestRaw* rawRequest = &server->rawRequests[connectionId];
    WebRequest request;
    ESP8266WebServer_InitRequest(&request, rawRequest);

    // Return with "400 Bad Request" if some part of the request is too long
    if (rawRequest->tooLong) {
#ifdef RESPONSE_HAS_BODY
        ESP8266WebServer_WriteResponse(&response, STATUS_BADREQUEST, CONTENTTYPE_PLAIN, NULL);
#else
        ESP8266WebServer_WriteResponse(&response, STATUS_BADREQUEST, CONTENTTYPE_PLAIN);
#endif
        ESP8266WebServer_OutputResponse(server, rawRequest, &response);
        ESP8266WebServer_ClearRawRequest(rawRequest);
        return 0;
    }

    WebServerRoute* routeMatch = NULL;
    for (unsigned int r = 0; r < server->routeCount; r++) {
        WebServerRoute* routePtr = &(server->routes[r]);
        int match = ESP8266WebServer_TestRouteMatch(routePtr, &request);
        if (match) {
            routeMatch = routePtr;
            break;
        }
    }

    // Return with "404 Not Found" if route not found for request
    if (routeMatch == NULL) {
#ifdef RESPONSE_HAS_BODY
        ESP8266WebServer_WriteResponse(&response, STATUS_NOTFOUND, CONTENTTYPE_PLAIN, NULL);
#else
        ESP8266WebServer_WriteResponse(&response, STATUS_NOTFOUND, CONTENTTYPE_PLAIN);
#endif
        ESP8266WebServer_OutputResponse(server, rawRequest, &response);
        ESP8266WebServer_ClearRawRequest(rawRequest);
        return 0;
    }

    // Handle request with handler function if route found
    routeMatch->handler(routeMatch, &request, &response);
    ESP8266WebServer_OutputResponse(server, rawRequest, &response);
    ESP8266WebServer_ClearRawRequest(rawRequest);
    return 1;

}

static void ESP8266WebServer_OutputResponse(ESP8266WebServer* server, const WebRequestRaw* rawRequest, WebResponse* response) {

    WebResponseStatusCode responseStatusCode = response->statusCode;
    WebResponseContentType responseContentType = response->contentType;
#ifdef RESPONSE_HAS_BODY
    const char* responseBody = response->body;
#endif

    if (response->tooLong) {
        responseStatusCode = STATUS_INTERNALERROR;
        responseContentType = CONTENTTYPE_PLAIN;
#ifdef RESPONSE_HAS_BODY
        responseBody = NULL;
#endif
    }

    size_t messageLength = 0;

    /* Status + reason phrase */

    char responseFirstLine[40];
    const char* reasonPhraseStr;
#ifdef USE_REASON_PHRASES_IN_RESPONSE
    switch (responseStatusCode) {
        case STATUS_OK:
            reasonPhraseStr = REASON_PHRASE_OK;
            break;
        case STATUS_BADREQUEST:
            reasonPhraseStr = REASON_PHRASE_BADREQUEST;
            break;
        case STATUS_FORBIDDEN:
            reasonPhraseStr = REASON_PHRASE_FORBIDDEN;
            break;
        case STATUS_NOTFOUND:
            reasonPhraseStr = REASON_PHRASE_NOTFOUND;
            break;
        case STATUS_INTERNALERROR:
            reasonPhraseStr = REASON_PHRASE_INTERNALERROR;
            break;
        default:
            reasonPhraseStr = EMPTY_STR;
            break;
    }
#else
    reasonPhraseStr = EMPTY_STR;
#endif
    messageLength += sprintf(responseFirstLine, RESPONSE_FIRST_LINE, responseStatusCode, reasonPhraseStr);

    /* Content type header*/

#ifdef USE_CONTENT_TYPE_HEADER_IN_RESPONSE
    const char* contentType;
    switch (responseContentType) {
        case CONTENTTYPE_PLAIN:
        default:
            contentType = CONTENT_TYPE_PLAIN;
            break;
        case CONTENTTYPE_JSON:
            contentType = CONTENT_TYPE_JSON;
            break;
        case CONTENTTYPE_XML:
            contentType = CONTENT_TYPE_XML;
            break;
    }
    char contentTypeHeader[40];
    messageLength += sprintf(contentTypeHeader, HEADER_CONTENT_TYPE, contentType);
#endif

    /* Sending response */

#ifdef RESPONSE_HAS_BODY
    if (responseBody)
        messageLength += strlen(responseBody);
#endif
    messageLength += (sizeof(CRLF) - 1); // \r\n at the end

    char atCmd[30];
    sprintf(atCmd, AT_CMD_CIPSEND_x_x, rawRequest->connId, (unsigned long) messageLength);
    server->outputWriter(atCmd);

    server->outputWriter(responseFirstLine);
#ifdef USE_CONTENT_TYPE_HEADER_IN_RESPONSE
    server->outputWriter(contentTypeHeader);
#endif
    server->outputWriter(CRLF);
#ifdef RESPONSE_HAS_BODY
    if (response->body)
        server->outputWriter(response->body);
#endif

    sprintf(atCmd, AT_CMD_CIPCLOSE_x, rawRequest->connId);
    server->outputWriter(atCmd);

}


static int ESP8266WebServer_TestRouteMatch(WebServerRoute* route, WebRequest* request) {
    if (!(request->method & route->method))
        return 0;
    return ESP8266WebServer_TestMatchAndGetParameter(route, request, 0, NULL);
}

int ESP8266WebServer_TestMatchAndGetParameter(const WebServerRoute* route, const WebRequest* request, unsigned int paramIndex, int* paramValue) {

    const char* requestUrl = request->url;
    const char* routeUrl = route->url;

    int currentParamIndex = 0;

    while (*routeUrl != '\0') {

        if (*routeUrl == '/' && routeUrl[1] != '\0') {

            if ((strlen(routeUrl) >= 3) /* Integer param */
                && (strncmp(routeUrl, "/:i", 3) == 0)
                && (routeUrl[3] == '/' || routeUrl[3] == '\0')) {

                routeUrl += 3;
                requestUrl++;

                currentParamIndex++;
                if (currentParamIndex == paramIndex)
                    *paramValue = 0;

                int charsFound = 0;
                while (*requestUrl >= '0' && *requestUrl <= '9') {
                    if (currentParamIndex == paramIndex) {
                        *paramValue *= 10;
                        *paramValue += *requestUrl - '0';
                    }
                    charsFound++;
                    requestUrl++;
                }

                if (charsFound == 0)
                    return 0;

            } else { /* Char match */
                do {
                    requestUrl++;
                    routeUrl++;
                } while ((*requestUrl == *routeUrl && !(*routeUrl == '/' || *routeUrl == '\0')));
            }

        } else if (*routeUrl == '/' && routeUrl[1] == '\0') {
            break;
        } else {
            return 0;
        }

    }

    if (requestUrl[0] == '\0')
        return 1;
    if (requestUrl[0] == '/' && requestUrl[1] == '\0')
        return 1;
    if (requestUrl[0] == '#' || requestUrl[0] == '?')
        return 1;

    return 0;

}

#ifdef RESPONSE_HAS_BODY

void ESP8266WebServer_WriteResponse(WebResponse* response, WebResponseStatusCode statusCode, WebResponseContentType contentType, const char* body) {
#else
    void ESP8266WebServer_WriteResponse(WebResponse* response, WebResponseStatusCode statusCode, WebResponseContentType contentType) {
#endif
    response->statusCode = statusCode;
    response->contentType = contentType;
#ifdef RESPONSE_HAS_BODY
    if (body != NULL) {
        if (!strlimcpy(response->body, body, MAX_LENGTH_OF_REP_BODY))
            response->tooLong = 1;
    } else {
        response->body[0] = '\0';
    }
#endif
}

static void ESP8266WebServer_InitResponse(WebResponse* response) {
    response->statusCode = STATUS_INTERNALERROR;
    response->contentType = CONTENTTYPE_PLAIN;
#ifdef RESPONSE_HAS_BODY
    response->body[0] = '\0';
    response->tooLong = 0;
#endif
}

static void ESP8266WebServer_InitRequest(WebRequest* request, WebRequestRaw* requestRaw) {

    // Fill 'method' member
    if (strncmp(requestRaw->firstLine, METHOD_STR_GET, (sizeof(METHOD_STR_GET) - 1)) == 0)
        request->method = METHOD_GET;
    else if (strncmp(requestRaw->firstLine, METHOD_STR_POST, (sizeof(METHOD_STR_POST) - 1)) == 0)
        request->method = METHOD_POST;
    else
        request->method = METHOD_UNKNOWN;

    // Fill 'url' member
    const char* urlPart = strchr(requestRaw->firstLine, ' ') + 1;
    const char* protocolPart = strchr(urlPart, ' ') + 1;
    size_t urlLength = protocolPart - urlPart - 1;
    if (!strnlimcpy(request->url, urlPart, urlLength, MAX_LENGTH_OF_REQ_URL))
        requestRaw->tooLong = 1;

#ifdef GET_REQUEST_BODY
    // Fill 'body' member
    if (!strlimcpy(request->body, requestRaw->body, MAX_LENGTH_OF_REQ_BODY))
        requestRaw->tooLong = 1;
#endif

}