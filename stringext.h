#ifndef ESP8266WEBSERVER_STRINGEXT_H
#define ESP8266WEBSERVER_STRINGEXT_H

#include <stdio.h>

int strlimcat(char* destination, const char* source, size_t maxlength);
int strlimcpy(char* destination, const char* source, size_t maxlength);
int strnlimcpy(char* destination, const char* source, size_t num, size_t maxlength);

#endif //ESP8266WEBSERVER_STRINGEXT_H
